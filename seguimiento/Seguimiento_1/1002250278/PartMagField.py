import numpy as np
import matplotlib.pyplot as plt

class ParticleMagField:
    """
    Recibe:
    m: la masa de la particula en kg, obviamente no cero
    q: la carga de la particula en C, no sirven particulas neutras
    E: la energia de la particula en eV
    pol: angulo polar de la velocidad en radianes
    B: intensidad del campo magnetico en Tesla
    """
    def __init__(self, m, q, E, pol, B):
        self.q=q
        self.m=m
        self.E=E/6.242e18
        self.pol=pol
        self.B=B    
        try: 
            self.w = self.q*self.m/self.B
            self.t=np.linspace(0,7*2*np.pi/self.w,10000)
            self.v=np.sqrt(2*np.abs(self.E)/self.m)
        except ZeroDivisionError as e:
            print('Masa no puede ser cero! Ni el campo! Tampoco la carga!')

    def Velini(self):
        vox=self.v*np.sin(self.pol)
        voy=0
        voz=self.v*np.cos(self.pol)
        return vox,voy,voz
    
    def Pos(self):
        X=self.Velini()[0]/self.w *np.sin(self.w*self.t)
        Y=self.Velini()[0]/self.w*(np.cos(self.w*self.t)-1)
        Z=self.Velini()[2]*self.t
        return X, Y, Z
    
    def Vel(self):
        Vx=self.Velini()[0]*np.cos(self.w*self.t)
        Vy=-self.Velini()[0]*np.sin(self.w*self.t)
        Vz=self.Velini()[2]
        return Vx, Vy, Vz
    
    def PosGraph(self):
        fig=plt.figure()
        ax= fig.add_subplot(111, projection='3d')
        ax.plot(self.Pos()[0],self.Pos()[1],self.Pos()[2])
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        ax.set_title('Trayectoria de particula cargada en Campo Magnetico cste')
        plt.savefig('GraphPartMagField.jpg')
        


