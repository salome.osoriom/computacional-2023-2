from Ising_model import IsingModel2D
if __name__== '__main__':
    
    
    ising_model = IsingModel2D(L_values=[2, 4, 6], T_min=1, T_max=5.0, num_T=50, k_B=1.0, J=1.0)
    C_dict, magnetization_dict = ising_model.simulate(num_steps=1000)

    ising_model.plot_combined(C_dict, magnetization_dict)