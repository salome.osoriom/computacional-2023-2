La libreria para realizar el ajuste de curva es 

"ajuste_ebo" 

La libreria tiene una clase que se llama "ajuste_curva", la cuál se instancia con la dirección del
archivo csv:

from ajuste_ebo import ajuste_curva
data = ajuste_curva(path)

La clase tiene varios métodos para realizar el plot con la curva ajustada dependiendo el tipo
y otros métodos que utilizan internamente. 

Ejemplo:  

data.curva_gauss(0.10)
data.curva_tricube(10)
data.curva_epanechnikov(10)

Estos devuelven un plot cada uno.

Adicional hay un método para mirar la comparación entre los tres métodos 

data.plot_comparar(10)


Todos estos métodos reciben el parametro 'h' , y en particular para el
kernel gaussiano en base a este h calculo el sigma.



Intenté tratar de que el archivo a analizar lo tomara en base a una URL,
como dice el parcial pero no estaba funcionando la manera como encontré en
internet que simplemente era pasarle la url en vez del path, pero hice pruebas
y no funcionó.