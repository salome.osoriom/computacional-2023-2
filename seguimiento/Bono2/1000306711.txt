Librería 1 (Integral con el método MonteCarlo):

pip install AVOlibraryCP

###########

from AVOlibraryCP import MonteCarloIntegration
import numpy as np

def f(x): #Función a integrar
    return x**2 * np.cos(x)

a = 0 #Límites de integración.
b = np.pi
N=50000 #Número de iteraciones.

monte_carlo = MonteCarloIntegration(f, a, b) 
exact_integral = monte_carlo.calculate_exact_integral() #Cálcula la integral exacta.
approx_integral = monte_carlo.calculate_monte_carlo_integral(N) #Calcula la integral con el método MonteCarlo con N iteraciones.
monte_carlo.figplot(N) #Grafica una comparación del resultado VS el número de iteraciones (Hasta el número N).



Librería 2 (Ajuste de datos de COVID-19):

pip install AVOlibrary2CP 

###########

from AVOlibrary2CP import GaussianKernel
import pandas as pd

sigma=2 #Este es un parámetro del ajuste, mientras más alto más suave será la curva.
data=pd.read_csv('datacovid.csv') #Aquí debe ir el archivo con los datos de covid (debe ser el que está en gitlab).
ajuste=GaussianKernel(data,sigma) .
ajuste.plot() # Grafica el ajuste.