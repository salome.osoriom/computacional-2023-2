import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


class Sist_termodinámico:
    """
    La clase representa un sistema de partículas simulado utilizando el método de Montecarlo en un 
    potencial de Lennard-Jones. 
    
    Params
    
    - num_particulas : Número de partículas
    - temperatura: Temperatura del sistema
    - num_pasos: Número de pasos en la simulación de montecarlo
    - epsilon y sigma: Parámetros del potencial de Lennard-Jones (Los suponemos 1)
    
    
    Return
    
    Retorna la gráfica de la evolución de la energía a medida que el sistema se reorganiza en función 
    de las condiciones termodinámicas
    
    """

    
    def __init__(self, num_part, temp, num_pasos, epsilon=1.0, sigma=1.0):
        self.num_part = num_part
        self.temp = temp
        self.num_pasos = num_pasos
        self.epsilon = epsilon
        self.sigma = sigma
        self.energies = []

    def energy(self, pos):
        dist = np.linalg.norm(pos[:, None] - pos, axis=2)
        
        np.fill_diagonal(dist, 1)  # Evitar la interacción consigo misma
        
        #Energía del potencial de Lennard-Jones
        energia = 4 * self.epsilon * ( (self.sigma / dist) ** 12 - (self.sigma / dist) ** 6 ) 
        
        return 0.5 * np.sum(energia) 

    def system_evolution(self):
        
        #Posición 3D inicial de las partículas
        pos = np.random.rand(self.num_part, 3)
        
        #Energía inicial
        energy_act = self.energy(pos) 

        for i in range(self.num_pasos):
            
            #Seleccionamos una partícula aleatoriamente y calculamos un desplazamiento aleatorio
            particle = np.random.randint(0, self.num_part)
            displacement = np.random.uniform(-0.1, 0.1, size=3)

            #Calculamos las nuevas posiciones y las nuevas energías
            new_pos = np.copy(pos)
            new_pos[particle] += displacement
            new_energy = self.energy(new_pos)

            #Calculamos la diferencia (delta) entre energías
            delta_E = new_energy - energy_act

            #Aceptar o rechazar el movimiento para minimizar la energía
            if delta_E < 0 or np.random.rand() < np.exp(-delta_E / self.temp):
                pos = new_pos
                energy_act = new_energy
            
            self.energies.append(energy_act)  # Registrar la energía en este paso
            
        self.final_pos = pos


    def plot_energies(self):
        plt.figure(figsize=(10, 6))
        plt.plot(self.energies,marker='.' ,color='b', linewidth=0.3)
        plt.xlabel('Paso de Montecarlo')
        plt.ylabel('Energía del Sistema (Normalizada)')
        plt.title('Evolución de la Energía del Sistema')
        plt.grid(True)
        plt.savefig('Evolución de la Energía del Sistema.png')
        plt.show()
        
    def plot_final_pos(self):
        
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        
        
        ax.scatter(self.final_pos[:,0], self.final_pos[:,1], self.final_pos[:,2])

        ax.set_title('Configuración del sistema de menor energía')
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        plt.savefig('Configuración del sistema de menor energía')
        plt.show()
        
    def Run(self):
        self.system_evolution()
        self.plot_energies()
        self.plot_final_pos()