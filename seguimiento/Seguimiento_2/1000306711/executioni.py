from integral import MonteCarloIntegration
import numpy as np

def f(x):
    return np.tanh(x/7)#x**2 * np.cos(x)

a = 0
b =1#np.pi
N=50000

if __name__ == "__main__":
    monte_carlo = MonteCarloIntegration(f, a, b)
    exact_integral = monte_carlo.calculate_exact_integral()
    approx_integral = monte_carlo.calculate_monte_carlo_integral(N)
    monte_carlo.figplot(N)