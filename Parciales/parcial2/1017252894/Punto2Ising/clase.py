import numba as nb
import numpy as np
from tqdm import tqdm
from matplotlib.colors import ListedColormap
import matplotlib.pyplot as plt
import imageio

@nb.jit(nopython=True)
def _Probabilidad_cambio(matriz,i,j,T,J=1,H=0):
    k_b = 1#1.38e-23
    N = len(matriz)
    spin = matriz[i,j]

    vecinos = matriz[(i+1) % N, j] + matriz[i, (j+1) % N] + matriz[(i-1) % N, j] + matriz[i, (j-1) % N]
    E_i = -J*spin*vecinos - H*spin

    E_f = -E_i
    D_E = E_f-E_i

    aleatorio = np.random.random()
    if D_E < 0 or aleatorio<np.exp(-D_E/(k_b*T)):
        matriz[i,j] *= -1

#------------------------------------------------------------------------------------------------------------------------------
class Ising:
    def __init__(self,L,T,iteraciones=20) -> None:
        self.iteraciones = iteraciones
        self.L = L
        self.T = T
        self.material = np.random.choice([1,-1],size=[L,L])



    def Probabilidad_cambio(self,matriz,i,j,T,J=1,H=0):
        _Probabilidad_cambio(matriz,i,j,T,J=1,H=0)


    def Simulacion(self):
        Frames = []
        for iter in tqdm(range(self.iteraciones)):
            for i in range(self.L):
                for j in range(self.L):
                    self.Probabilidad_cambio(self.material,i,j,self.T,H=0)

            colors = ['yellow','black' ]  # Azul para 1, Rojo para -1
            cmap = ListedColormap(colors)

            fig, ax = plt.subplots()
            ax.imshow(self.material,cmap=cmap)
            plt.title(f'Gráfico {iter+1}')

            fig.canvas.draw()
            image = np.array(fig.canvas.renderer.buffer_rgba())
            Frames.append(image)
            plt.close()
        imageio.mimsave('Simulacion_Ising.gif', Frames,loop=0)