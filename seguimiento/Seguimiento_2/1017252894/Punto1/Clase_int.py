import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad

class MontecarloInt:
    """
    Clase para calcular el valor de una integral por el método de Montecarlo 
    a y b: Intervalo donde está definida la función
           Deben ser números reales
    función: función que se desea integrar 
    """
    
    def __init__(self,a:float,b:float,funcion):
        try:
            float(a)
            float(b)
        except:
            print("a y b deben ser valores reales")
            print('Reinicie el programa')
            exit()

        self.a = a
        self.b = b
        self.funcion = funcion

    def analiica(self):
        """
        Este método calcula el valor de la integral usando la librería scipy
        Este valor se toma como el valor real
        """
        real = quad(self.funcion, self.a, self.b)
        return real[0]


    def solve(self,N=100000):
        """
        Este método resuelve la integral
        Retorna un valor real que es el resultado de la integral
        N: Número de iteraciones
        """
        try:
            int(N)
        except:
            print('N debe ser un numero entero')
            print('Reinicie el programa')
            exit()

        if (N <= 0):
            print('N deber ser un munero mayor que cero')
            print('Reinicie el programa')
            exit()

        puntos = np.random.rand(N)*(self.b-self.a) + self.a
        # Se generan N puntos aleatorios en el intervalo de solucion

        vals = np.array([self.funcion(i) for i in puntos] )
        # Se calcula el valor de la funcion para cada punto aleatorio

        val = vals.mean()*(self.b-self.a)
        # Se calcula el promedio de estos valores y se multiplica 
        # el ancho del intervalo (b-a)
        
        return val
    
    def simulacion(self,iter_max):
        try:
            int(iter_max)
        except:
            print('El numero de iteraciones maximas debe ser un entero')
            print('Reinicie el programa')
            exit()
            
        if (iter_max <= 0):
            print('El numero de iteraciones maximas deber ser un munero mayor que cero')
            print('Reinicie el programa')
            exit()

        self.iteraciones = np.linspace(100,iter_max,200)
        self.vals = [self.solve(int(i)) for i in self.iteraciones]
        return self.vals
        
        
        
    def grafica(self):
        plt.axhline(self.analiica(),ls='--',color='k',alpha=0.5,label='Valor Real')
        plt.plot(self.iteraciones,self.vals,label='Valor calculado con Montecarlo',color='r',lw=1)
        
        plt.legend()
        plt.grid()
        plt.xlabel('Iteraciones')
        plt.ylabel('Valor de la integral')
        plt.title('Valor de la integral en funcion del número de iteraciones')
        #plt.show()
        #plt.savefig('Integral Montecarlo.jpg')
        plt.savefig('Borrar.jpg')
    
