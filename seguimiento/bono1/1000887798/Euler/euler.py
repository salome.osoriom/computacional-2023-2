import numpy as np
import matplotlib.pyplot as plt 
from sympy import symbols, Function, Eq, dsolve

class edo:
    """Clase para resolver ecuaciones diferenciales ordinarias
    por el método de Euler y Runge Kutta 4.


    Los parámetros de la clase son:
    a: límite inferior del intervalo
    b: límite superior del intervalo
    n: número de subintervalos
    y0: condición inicial
    f: función de la ecuación diferencial
    punto: punto a evaluar la solución analítica

    """
    #Metodo constructor
    def __init__(self, a, b, n, y0,f, punto=False):

        """Método constructor de la clase edo"""

        self.a = a 
        self.b = b
        self.n = n
        self.y0 = y0
        self.punto = punto  #Se iniciliazan los atributos
        self.x = []
        self.y = []
        self.f = f
        self.k1 = 0
        self.k2 = 0
        self.k3 = 0
        self.k4 = 0


    #Método para econtrar "h"
    def h(self):

        """Método para encontrar el tamaño de los subintervalos"""

        try:
            if self.punto:
                return (self.punto-self.a)/self.n
            else:
                return (self.b-self.a)/(self.n-1)
        except :
            print("Error al calcular 'h' revise los parámetros")
    
    def X(self):

        """Método para econtrar los valores de x"""

        try:
            self.n
            if self.punto:
                # self.x = np.arange(self.a, self.punto+self.h(), self.h())
                self.x = np.linspace(self.a, self.punto+self.h(), self.n+1)
                
            else:
                #self.x = np.arange(self.a, self.b+self.h(), self.h())
                self.x = np.linspace(self.a, self.b+self.h(), self.n+1)
            
        
            return self.x
        except :
            print("Error al calcular 'X' revise los parámetros")
    
    def euler(self):

        """Método para resolver la ecuación diferencial\
              por el método de Euler"""
        
        try:

            self.X() 
            self.y = [self.y0]

            for i in range(self.n):
                self.y.append(self.y[i]+self.h()\
                            *self.f(self.x[i], self.y[i]))
            return self.x,self.y
        except :
            print("Error al calcular la solución por el\
                  método 'euler', revise los parámetros")

    def solanali(self):

        """Método para resolver la ecuación diferencial\
              analiticamente con sympy"""
        
        try:
            x_sym = symbols('x')
            y_sym = Function('y')(x_sym)
            
            
            diff_eq = Eq(y_sym.diff(x_sym), self.f(x_sym, y_sym))
            sol = dsolve(diff_eq, y_sym, ics={y_sym.subs(x_sym, self.a): self.y0})
            y_values = [sol.rhs.subs(x_sym, x_val) for x_val in self.X()]
            
            return self.x, y_values
        except:
            print("Error al calcular la solución analítica,\
                  revise los parámetros")
    
    def figPLot(self):

        """Método para graficar la solución de la ecuación diferencial/
        por los métodos de Euler, RK4 y analíticamente"""

        try:
            plt.style.use("seaborn")
            plt.figure(figsize=(10,5))
            plt.title("Solución de EDO por diferentes métodos")
            xeuler , yeuler = self.euler()
            xRK4 , yRK4 = self.RK_4()
            xanalitica , yanalitica = self.solanali()
            plt.plot(xeuler,yeuler, label="Euler")
            plt.plot(xRK4,yRK4, label="RK4")
            plt.plot(xanalitica,yanalitica, label="Analitica")
            plt.legend()
            plt.savefig("solEuler.png")
            return True
        except:
            return print("Error al graficar, revise la ecuación diferencial")

    

    def RK_4(self, w1=False, w2=False, w3=False, w4=False):

        """ Método para resolver la ecuación diferencial\
            por el método de Runge Kutta 4.
            Se deja por defecto los w_i se toman los de
            RG-4, pero se deja la opción de poder ingresarlos
            por si se desea usar valores diferentes para
            estos coeficientes."""
        

        try:

            self.w1 = w1
            self.w2 = w2
            self.w3 = w3
            self.w4 = w4

            self.X()
            self.y = [self.y0]

            for i in range(self.n):
                k1 = self.f(self.x[i], self.y[i])
                k2 = self.f(self.x[i]+self.h()/2, self.y[i]+self.h()*k1/2)
                k3 = self.f(self.x[i]+self.h()/2, self.y[i]+self.h()*k2/2)
                k4 = self.f(self.x[i]+self.h(), self.y[i]+self.h()*k3)
                self.y.append(self.y[i]+self.h()*(k1+2*k2+2*k3+k4)/6)

            return self.x,self.y
        except:
            print("Error al calcular la solución por el\
                  método 'RK4', revise los parámetros")
