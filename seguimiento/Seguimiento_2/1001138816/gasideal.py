import numpy as np
import matplotlib.pyplot as plt

class VelocidadGas:
    '''
    Esta clase realiza una simulación de Montecarlo de la distribución de velocidades en un gas ideal contenido en una caja (2D)
    Es necesario ingresar los siguientes parámetros:
    num_particulas: Entero. Número total de partículas en el sistema.
    niter: Entero. número de iteraciones con el que se hace la simulación.
    temperatura: Real. temperatura a la que está el sistema.
    masa: real distinto de cero. masa total del sistema.
    lado: real distinto de cero. longitud de lado de la caja (cuadrada) en la que está contenida el gas

    Los suguientes parámetros son opcionales.
    kb: real. constante de boltzmann en la unidades en que se ingresen los demás parámetros. Se asume kb = 1
    dt: real. intervalo de tiempo en que evoluciona el sistema, se asume dt = 0.01 s

    La clase contiene los siguientes métodos:
    mc_vel: se realiza la simulación de montecarlo de la distribución de velocidades. retorna dicha distribución
    plot_dis: grafica la distribución simulada y la distrubción real del sistema (Maxwell Boltzmann)
    '''

    def __init__(self,num_particulas:int,niter:int,temperatura:float,masa:float,lado:float, kb=1,dt =0.01):
        self.np = num_particulas
        self.ni = niter
        self.T = temperatura
        self.m = masa
        self.l = lado
        self.kb = kb
        self.dt =dt

    def mc_vel(self):
        try:
            np.random.seed(1)
            N = self.np
            L = self.l
            dt = self.dt
            n_steps = self.ni
            T = self.T
            k = self.kb
            # se inicializan las posiciones y velocidades
            pos = np.random.rand(N, 2)
            vel = np.random.randn(N, 2)

            #  array donde se guardaran las velocidades en cada iteracion
            vel_all = np.zeros((n_steps, N, 2))

            for i in range(n_steps):
                # actualizar las posiciones
                pos += vel * dt

                # si la posición se choca con una pared de la caja, cambia su trayectoria
                for j in range(2):
                    mask = np.logical_or(pos[:, j] < 0, pos[:, j] > L)
                    vel[mask, j] *= -1

                # genrera nueva velocidad
                new_vel = np.random.randn(N, 2)

                # Calcular el cambio de energia
                old_e = 0.5*np.sum(vel**2, axis=1)
                new_e = 0.5*np.sum(new_vel**2, axis=1)
                delta_e = new_e - old_e

                # calcula la probabilidad de Boltzmann
                boltzmann = np.min(np.array([np.ones(np.shape(delta_e)),np.exp(-delta_e / (k * T))]))

                # si cumple con la probabilidad, se acepta
                accept = np.random.random(N) < boltzmann
                vel[accept] = new_vel[accept]

                vel_all[i] = vel

            # Plot the velocity distribution at the end of the simulation
            final_vel = vel_all[-1]
            v2 = np.sqrt(np.sum(final_vel**2, axis=1))
            return v2
        except:
            print('Ha surgido un error en la simulación de montecarlo, \n revise los parámetros de entrada')

    def plot_dist(self):
        try:
            # histograma de datos simulados
            v2 = self.mc_vel()
            plt.figure(figsize=(8,8))
            plt.grid()
            plt.hist(v2, bins=50, density=True, alpha=0.8, label='Simulados')

            # distribucion M-B
            v = np.linspace(0, 10, 1000)
            c = 0.25
            f_maxwell = np.sqrt((1/(2*np.pi*self.kb*self.T)))**3*4*np.pi*(v+c)**2 * np.exp(-(v+c)**2/(2*self.T))
            plt.plot(v, f_maxwell,c='deeppink', label='Maxwell-Boltzmann')
            plt.xlabel('Speed')
            plt.ylabel('Probability density')
            plt.legend()
            plt.savefig('MC_gasideal.png')
            plt.show()
        except:
            print('Ha surgido un error al graficar, \n revise los parámetros de entrada')
