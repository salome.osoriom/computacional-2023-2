from __future__ import division
import numpy as np
from numpy.random import rand
import matplotlib.pyplot as plt
from tqdm import tqdm  # Importa tqdm para la barra de progreso
from scipy.optimize import curve_fit


class isingModel:

    def __init__(self,N,nt,paso,a,b):

        self.N = N #Tamaño de la rejilla de espines
        self.nt = nt #Número de puntos para poner en los plots
        self.paso = paso #Número de pasos para la simulación montecarlo
        self.a  = a #temperatura inicial
        self.b = b #temperatura final


    def initialstate(self,N):   
        """Este método genera un arreglo de 1, -1 de forma aleatoria"""
        config0 = 2*np.random.randint(2, size=(self.N,self.N))-1
        return config0


    def mcmove(self,config, beta):
        """Simulación de montecarlo"""
        for i in range(self.N):#Se itera sobre filas y columnas
            for j in range(self.N):
                    a = np.random.randint(0, self.N)
                    b = np.random.randint(0, self.N)
                    s =  config[a, b] #Escogemos un espin con posición (a,b)
                    nb = config[(a+1)%self.N,b] + config[a,(b+1)%self.N] + config[(a-1)%self.N,b] + config[a,(b-1)%self.N] #se calcula la energía de los vecinos al espin seleccionado
                    cost = 2*s*nb #energía que se usará en la probabilidad
                    if cost < 0:
                        s *= -1 #El espin cambia si la energía es menor que cero 
                    elif np.random.rand() < np.exp(-cost*beta):
                        s *= -1 #El espin cambia si la exponencial es mayor a un número aleatorio
                    config[a, b] = s #Si no se cumplen las condiciones el espin se mantiene
        return config


    def calcEnergy(self,config):
        """Energia para una confiduración es espines"""
        energy = 0
        for i in range(len(config)):
            for j in range(len(config)):
                S = config[i,j]
                nb = config[(i+1)%self.N, j] + config[i,(j+1)%self.N] + config[(i-1)%self.N, j] + config[i,(j-1)%self.N]
                energy += -nb*S
        return energy/4


    def calcMag(self,config):
        """Magnnetización"""
        mag = np.sum(config)
        return mag
    
    def calcular(self):
        """Este método calcula la magnetización y el calor específico en un cierto rango de temperaturas"""

        T       = np.linspace(self.a, self.b, self.nt); #Arreglo de temperaturas
        E,M,C = np.zeros(self.nt), np.zeros(self.nt), np.zeros(self.nt) #Listas vacias para las variables a calcular: energía, magnetización, calor especifico
        n1  = 1/(self.paso*self.N**2)
        n2 = 1/(self.paso**2*self.N**2) #Normalización

        for j in tqdm(range(self.nt), desc='Simulando Temperaturas'): #Se itera en el rango de temperaturas
            E1 = M1 = E2 = M2 = 0 #para cada valor de temperatura se inician las variables en cerp
            config = self.initialstate(self.N) # Estado inicial
            iT=1/T[j] #parametro beta
            iT2=iT**2
     

            for i in range(self.paso):
                self.mcmove(config, iT)  #Se mueve la configuración inicial     
                Ene = self.calcEnergy(config) #Se calcula la energía
                Mag = self.calcMag(config) # Se calcula la magnetización

                E1 = E1 + Ene #Nuevo valor de energía
                M1 = M1 + Mag #nuevo valor de magnetización
                M2 = M2 + Mag*Mag #suma de los cuadrados
                E2 = E2 + Ene*Ene

            E[j] = n1*E1 #Energía normalizada por cada iteración
            M[j] = n1*M1 #magnetización normalizada por cada iteración
            C[j] = (n1*E2 - n2*E1*E1)*iT2 #Calor específico normalizado

        return T,E,M,C

    def plot(self):
        T,E,M,C = self.calcular() #Se almacenan las variables
        
        plt.figure()
        plt.scatter(T, abs(M), color='darkblue') #Se grafica el valor absoluto para ver la tendencia
        plt.xlabel("Temperatura")  
        plt.ylabel("magnetizacion") 
        plt.title(f"magnetizacion con L = {str(self.N)}")
        plt.savefig(f"plots/Magnetizacion_L{str(self.N)}.png")

        plt.figure()
        plt.scatter(T, C, marker='o', color='darkblue')
        plt.xlabel("Temperatura")  
        plt.ylabel("Calor especiifico")
        plt.title(f"Calor específico con L = {str(self.N)}")
        plt.savefig(f"plots/calor_L{str(self.N)}.png")
