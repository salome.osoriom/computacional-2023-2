from euler import EDO


#NOTA 5
def f(y,x):
    return x*3/(y*2)-5*x-y*x

if __name__ == "__main__":
    edo1=EDO(0,1,50,[0,1],f)
    edo1.figplot()