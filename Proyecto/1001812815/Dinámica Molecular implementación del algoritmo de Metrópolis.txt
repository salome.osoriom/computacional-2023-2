DINÁMICA MOLECULAR: IMPLEMENTACIÓN DEL ALGORITMO DE METRÓPOLIS


MARCO TEÓRICO:

El estudio de dinámica molecular y estados de equilibrio hace gran uso de los algoritmos de Monte Carlo para la simulación de perturbaciones
moleculares y la interacción atómica y eléctrica. De particular utilidad para el estudio de estados de equilibrio es el algoritmo de 
Monte Carlo Metropolis, esto se debe a que la generación de puntos nuevos se basa en el punto anterior y por ende la taza de aprobación
de perturbaciones por Metropolis emula mucho mejor las pequeñas variaciones que experimenta cada constituyente de una molécula, mientras
que un Monte Carlo general emula posiciones con una distribucion completamente arbitraria y aumenta el tiempo de computo comparado al 
Metropolis. 

Además, la simulación de gases interactuantes por medio de fuerzas de Van der Waals y Coulomb también se pueden simular por medio de 
Metropolis de manera más eficiente que a un Monte Carlo tradicional, por las mismas razones de las moléculas.

Naturalmente, el comportamiento de moléculas y gases interactuantes se ven principalmente afectados por la temperatura inicial del sistema.
La energía inicial de las moléculas, que en este caso solamente sería energía potencial dado los cambios aleatorios por Metropolis, y su temperatura están directamente implicados en la estadística de Maxwell-Boltzmann, estadística que rige el comportamiento estadistico de sistemas dinámicos en términos de estados de energía que ocupa la molécula tras cada variación de Metropolis. Esta estadística sería la condición de aprobación para las variaciones propuestas.

Si bien la dinámica de 2 cuerpos interactuantes tiene solucicón analítica, para 3 o más cuerpos la dinámica es numérica y necesariamente costosa computacionalmente. Monte Carlo Metropolis es uno de los métodos de solución numérica para sistemas de 3+ cuerpos, particularmente comunes en el mundo de la mecánica molecular.

Es también interesante hacer un estudio iterado para variadas temperaturas sobre una configuración particular, dada la aleatoridad de Montecarlo, puede llevar al sistema a llegar a diferentes mínimos locales y tener una idea del panorama de energía de la molécula a diferentes temperaturas.

El software VMD (Visual Molecular Dynamics) de acceso libre, se usa para la simulación visual de moléculas dada la información espacial de 
las componentes, información simulada a partir de Metropolis.


