import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.animation import PillowWriter
from celluloid import Camera

# Clase para representar partículas en el gas ideal
class Particle:
    def __init__(self, x, y, vx, vy):
        self.x=x
        self.y=y
        self.vx=vx
        self.vy=vy

class Ideal_Gas:
    '''
    Se modela en comportamiento de un grupo de partículas dentro de una caja, o bien, un cuadrado
    para 2 dimensiones, el conjunto de partículas es tratado como un gas ideal. se usa el modelo 
    de la distribución de velocidades de Maxwell-Boltzmann para determinar en qué posición estará 
    cada partícula (dada una posición y velocidad inicial).

    num_particles: número de partículas
    num_steps: pasos que dará la partícula desde la posición inicial, con velocidad v
    temperature: temperatura de la caja
    box_size: lado de la caja
    particle_mass: masa de cada partícula

    '''
    def __init__(self, num_particles, num_steps, temperature, box_size, particle_mass):
        self.n = num_particles
        self.steps = num_steps
        self.temp = temperature
        self.box = box_size
        self.mass= particle_mass
        self.k=1.38*1e-23

    # Función para simular el movimiento de las partículas
    def simulate_gas(self):
        self.particles = []
        for i in range(self.n):
            x = np.random.uniform(0, self.box)
            y = np.random.uniform(0, self.box)
            vx = np.random.normal(0, np.sqrt(2*self.k*self.temp/ self.mass)) #velocidad donde la distribución de velocidades es mayor
            vy = np.random.normal(0, np.sqrt(2*self.k*self.temp/ self.mass))
            self.particles.append(Particle(x,y,vx,vy))

    # Listas para almacenar datos para gráficos
        positions_x = []
        positions_y = []

        for i in range(self.steps):
            for particle in self.particles:
                particle.x += particle.vx
                particle.y += particle.vy

                # Aplicar condiciones de borde periódicas
                particle.x %= self.box
                particle.y %= self.box

                positions_x.append(particle.x)
                positions_y.append(particle.y)

        return positions_x, positions_y
    
    def plot(self):
        x,y=self.simulate_gas()
        plt.scatter(x,y, s=5)
        plt.xlim(0, self.box)
        plt.ylim(0, self.box)
        plt.xlabel('Posición X')
        plt.ylabel('Posición Y')
        plt.title('Simulación de un Gas Ideal')
        plt.savefig("grafico.png")
    
    def animated(self):

        fig=plt.figure()
        ax=plt.axes()
        ax.set_ylim(0, self.box)
        ax.set_xlim(0, self.box)

        camera=Camera(fig)
            
        for j in range(500):
            xlist,ylist=self.simulate_gas()

            ax.scatter(xlist,ylist,s=5)
            #plt.pause(0.1)
            camera.snap()
        
        animation=camera.animate()
        animation.save('gas.gif',fps=10)
    
    def run(self):

        try:
            self.simulate_gas()
        except ZeroDivisionError:
            print("Error al dividir por cero")

        try:
            self.plot()
        except ZeroDivisionError:
            print("Error al dividir por cero")
        
        try:
            self.animated()
        except ZeroDivisionError:
            print("Error al dividir por cero")

        try:
            self.simulate_gas()
        except:
            print("Error. Verifique los valores")
        
        try:
            self.plot()
        except:
            print("Error. Verifique los valores")
        
        try:
            self.animated()
        except:
            print("Error. Verifique los valores")


