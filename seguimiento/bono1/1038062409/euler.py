import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp


class EDO(object):
    """
    Este objeto permite resolver ecuaciones diferenciales ordinarias de primer orden
    utilizando el metodo de Euler y Runge-Kutta de orden 4.
    """
    def __init__(self,a,b,n,y0,f,punto=False):
        self.a = a
        self.b = b
        self.n=n 
        self.y0 = y0
        self.punto = punto
        self.x=[]
        self.y=[]
        self.f = f
        
        
    def h(self):
        """
        Paso de integracion
        """
        if self.punto:
            h = (self.b - self.a) / self.n
            hp = (self.punto - self.a) / self.n
            return h,hp
        
        else:
            return (self.b - self.a) / self.n
    
    def X(self):
        """
        Vector de puntos
        """
        if self.punto:
            self.x  = np.arange(self.a,self.b+self.h()[0],self.h()[0])
            self.xp = np.arange(self.a,self.punto+self.h()[1],self.h()[1])
            return self.x , self.xp
        
        else:
            self.x=np.arange(self.a,self.b+self.h(),self.h())
            return self.x
    
    def euler(self):
        """
        Metodo de Euler
        """
        if self.punto:
            self.X()
            self.y  =[self.y0]
            self.yp =[self.y0]
            
            for i in range(self.n):
                self.y.append(self.y[i]+self.h()[0]\
                    *self.f(self.x[i],self.y[i]))
                
                self.yp.append(self.yp[i]+self.h()[1]\
                    *self.f(self.xp[i],self.yp[i]))
                
            return self.x, self.y , [self.xp[-1],self.yp[-1]]
        
        else:
            self.X()
            self.y=[self.y0]
            
            for i in range(self.n):
                self.y.append(self.y[i]+self.h()\
                    *self.f(self.x[i],self.y[i]))
                
            return self.x, self.y
        
    
    def solanalitica(self):
        """
        Solucion analitica utilizando solve_ivp
        """
        if self.punto:
            self.X()
            solanaliticap = solve_ivp(self.f, [self.a, self.punto+self.h()[1]], [self.y0],t_eval=self.xp)
            solanalitica = solve_ivp(self.f, [self.a, self.b+self.h()[0]], [self.y0],t_eval=self.x)
            return solanalitica.t, solanalitica.y[0] , [self.xp[-1],solanaliticap.y[0][-1]]
        
        else:
            self.X()
            solanalitica = solve_ivp(self.f, [self.a, self.b+self.h()], [self.y0],t_eval=self.x)
            return solanalitica.t, solanalitica.y[0]
        
     
    def RK4(self):
        """
        Runge-Kutta de orden 4
        """
        if self.punto:
            self.X()
            self.y  =[self.y0]
            self.yp =[self.y0]
            
            for i in range(self.n):
                k1  = self.h()[0]*self.f(self.x[i],self.y[i])
                k1p = self.h()[1]*self.f(self.xp[i],self.yp[i])
                k2  = self.h()[0]*self.f(self.x[i]+self.h()[0]/2,self.y[i]+k1/2)
                k2p = self.h()[1]*self.f(self.xp[i]+self.h()[1]/2,self.yp[i]+k1p/2)
                k3  = self.h()[0]*self.f(self.x[i]+self.h()[0]/2,self.y[i]+k2/2)
                k3p = self.h()[1]*self.f(self.xp[i]+self.h()[1]/2,self.yp[i]+k2p/2)
                k4  = self.h()[0]*self.f(self.x[i]+self.h()[0],self.y[i]+k3)
                k4p = self.h()[1]*self.f(self.xp[i]+self.h()[1],self.yp[i]+k3p)
                self.y.append(self.y[i]+(k1+2*k2+2*k3+k4)/6)
                self.yp.append(self.yp[i]+(k1p+2*k2p+2*k3p+k4p)/6)
            
            return self.x, self.y , [self.xp[-1],self.yp[-1]]
        
        else:
            self.X()
            self.y=[self.y0]
            
            for i in range(self.n):
                k1 = self.h()*self.f(self.x[i],self.y[i])
                k2 = self.h()*self.f(self.x[i]+self.h()/2,self.y[i]+k1/2)
                k3 = self.h()*self.f(self.x[i]+self.h()/2,self.y[i]+k2/2)
                k4 = self.h()*self.f(self.x[i]+self.h(),self.y[i]+k3)
                self.y.append(self.y[i]+(k1+2*k2+2*k3+k4)/6)
            
            return self.x, self.y
                
    def figplot(self):
        """
        Grafica de la solucion analitica, euler y RK4
        """
        solanalitica=self.solanalitica()
        solEu=self.euler()
        solRK4=self.RK4()
             
        _ , ax = plt.subplots(figsize=(10,5))
        
        if self.punto:
            
            #Gráfica de la solución
            ax.plot(solanalitica[0],solanalitica[1],'b-',label='Sol_Analitica',markersize=10,linewidth=3)
            ax.plot(solEu[0],solEu[1],'y-',label='Sol_Euler',markersize=0.5,linewidth=2.2)
            ax.plot(solRK4[0],solRK4[1],'r-',label='Sol_RK4',markersize=1,linewidth=1.8)
            
            #Gráfica del punto
            ax.plot(solanalitica[2][0],solanalitica[2][1],'bo',label='Sol_Analitica_Punto',markersize=8)
            ax.plot(solEu[2][0],solEu[2][1],'yp',label='Sol_Euler_Punto',markersize=7)
            ax.plot(solRK4[2][0],solRK4[2][1],'r*',label='Sol_RK4_Punto',markersize=6)
         
            ax.legend()
            ax.set_xlabel('x')
            ax.set_ylabel('y')
            ax.set_title('Solucion EDO')
            ax.grid()
            plt.show()
            
        else:
            #Gráfica de la solución
            ax.plot(solanalitica[0],solanalitica[1],'ko',label='Sol_Analitica')
            ax.plot(solEu[0],solEu[1],'r--',label='Sol_Euler')
            ax.plot(solRK4[0],solRK4[1],'b-',label='Sol_RK4')
            ax.legend()
            ax.set_xlabel('x')
            ax.set_ylabel('y')
            ax.set_title('Solucion EDO')
            ax.grid()
            plt.show()
