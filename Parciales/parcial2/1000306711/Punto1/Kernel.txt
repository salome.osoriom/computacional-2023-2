pip install KernelAVO

############################

from KernelAVO import *
import pandas as pd

sigma=2 #Este es un parámetro del ajuste, mientras más alto más suave será la curva.

data=pd.read_csv("datacovid.csv") #Aquí debe ir el archivo con los datos de covid
ajuste=Kernel(data,sigma)
ajuste.plotgaussian() #Grafica el ajuste gaussiano
ajuste.plottricube() #Grafica el ajuste tricube
ajuste.plotepanechnikov() #Grafica el ajuste epanechnikov
ajuste.plotcomparation() #Grafica la comparación entre los 3 ajustes.