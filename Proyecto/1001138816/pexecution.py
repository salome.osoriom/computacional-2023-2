import numpy as np  # Para manejo de matrices
import os           # Para manejo de archivos
import VisOpy as vo # Librería

###################### entradas de abertura
L=10e-3     # Longitud de lado del espacio
N=1024      # Número de muestras      
rx = 1e-3   # Radiox de la abertura
ry = 1e-3 # Radioy de la abertura
###################### entradas de transmitancia
abertura = 'circle' # Tipo de abertura
fase = None     # Tipo de fase
setst = np.deg2rad(np.array([20,20]))       # Características de la fase tilt
setsf = 1000                                # Características de la fase focus
# fase personalizada
Pmax=3; 
fasep= lambda rho,thet: (Pmax*thet*(rho**2))/(4*np.pi)
setsp = None
aberrado = True                  # Considera o no aberraciones
npol = np.array([5,6])            # Aberraciones a considerar
coef = np.array([0.3,0.4])*1e-6 # Coeficientes de aberración
cAb= np.array([npol,coef])       # Características de las aberraciones
campo = [abertura,fasep,setsp,aberrado,cAb] # Características del campo
wl = 550e-9 # Longitud de onda
####################### entradas de propagacion de la luz
tip = 'Fraunhofer' # Tipo de propagador
dist = 100 # Distancia de propagación
####################### entradas de formacion de imagen
file = 'E.tif'
rut = os.path.realpath(file) 
SimIm = vo.formIm(L,N,rx,campo,wl,dist,tip,rut)
SimIm.plotConv()
