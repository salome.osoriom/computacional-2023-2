Proyecto: Generación de eventos para estadística del boson Z'

La idea sería realizar el análisis en torno al boson Z', 
desde la generación de datos hasta los análisis posteriores a este.
Se evaluaría la posibilidad de implementar machine learning para la selección de eventos.
