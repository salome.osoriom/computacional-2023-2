from class_QM import QM
import numpy as np
import sympy as sp



if __name__ == "__main__":
   
    a = -2/np.sqrt(3)
    b = 2/np.sqrt(3)
    N = 10000
    
    
    psi = lambda x : 2/np.pi *  1/(x**2+4)  ##Le paso la funcion de onda sin la fase que se anula la elevar al cuadrado

    qm = QM(psi, a, b, N)
    

    print(f"El valor de la probabilidad de encontrar la particula en el intervalo [{a:.4},{b:.4}] es:")
    print(f"el método montecarlo es : {qm.area(N):.3}")
    print(f"el método analítico es : {qm.analitica():.3}")

    # qm.plot_iteraciones(10, 10000)

    qm.plot_psi()

    

    