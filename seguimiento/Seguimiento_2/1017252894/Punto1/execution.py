import numpy as np
import matplotlib.pyplot as plt
from Clase_int import *


function = lambda x:x**2 * np.cos(x)

if __name__ == "__main__":
    a = 0
    b = np.pi # intervalo de la solución

    N = 100000 # Número máximo de iteraciones

    integral = MontecarloInt(a,b,function)

    integral.simulacion(N)
    integral.grafica()
