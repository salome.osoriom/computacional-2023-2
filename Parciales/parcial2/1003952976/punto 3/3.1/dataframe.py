import pandas as pd

class Join:
    def __init__(self):
        pass

    def leer(self):
        try:
            join_data_1= {
                'id': ['1', '2', '3', '4', '5'],
                'primer_nombre': ['Alex', 'Amy', 'Valentina', 'Alice', 'Lina'], 
                'apellido': ['Anderson', 'Ruales', 'Marin', 'Barbosa', 'Robles']}
        
            join_data_2= {
                'id': ['4', '5', '6', '7', '8'],
                'primer_nombre': ['Luis', 'Brian', 'Mariana', 'Marcela', 'Carlos'], 
                'apellido': ['Ruiz', 'Giralgo', 'Ortegon', 'Palacios', 'Gallego']}

            join_data_3= {
                'id': ['1', '2', '3', '4', '5', '7', '8', '9', '10', '11'],
                'test_id': [51, 15, 15, 61, 16, 14, 15, 1, 61, 16]}
        
            df1=pd.DataFrame(join_data_1)
            df2=pd.DataFrame(join_data_2)
            df3=pd.DataFrame(join_data_3)
        
            return df1, df2, df3
        except:
            print("error. La tarea Falló, verifique las entradas")
    
    def unir_en_filas(self):
        try:
            df1,df2,df3=self.leer()

            df_concat = pd.concat([df1, df2])
            df_concat.reset_index(drop=True, inplace=True)

            return df_concat

        except:
            print("error. La tarea Falló, verifique las entradas")
    
    def unir_en_columnas(self):
        try:
            df1,df2,df3=self.leer()
            df_concat = pd.concat([df1, df2], axis=1)

            return df_concat
        
        except:
            print("error. La tarea Falló, verifique las entradas")
        
    def merge_df(self):
        try:
            df1,df2,df3=self.leer()
            merged_df = pd.merge(pd.merge(df1, df2, on='id', how='outer'), df3, on='id', how='outer')

            return merged_df

        except:
            print("error. La tarea Falló, verifique las entradas")
    
    def merge_equal_df(self):
        try:
            df1,df2,df3=self.leer()
            merged_df = pd.merge(pd.merge(df1, df2, on='id', how='inner'), df3, on='id', how='inner')

            return merged_df

        except:
            print("error. La tarea Falló, verifique las entradas")
    

        
    


