import numpy as np
from intmontecarlo import intMC

if __name__ == "__main__":
    
    #Parametros del problema
    a = 0     # Limite inferior de la integral
    b = 1#np.pi # Limite superior de la integral (debe ser mayor que a)
    N = 50000 # Numero de puntos aleatorios (debe ser mayor que cero)
    
    # Funcion a integrar (debe ser continua en el intervalo [a,b])
    f = lambda x: np.tanh(x/7)#x**2*np.cos(x)
    
    intMC(f, a, b, N).run()
    SolucionMontecarlo = intMC(f, a, b, N).integrateMC()
    SolucionExacta = intMC(f, a, b, N).integrateEx()
 
    print("--"*20)
    print("Solucion por Monte Carlo:   {:.3f}".format(SolucionMontecarlo))
    print("Solucion exacta:            {:.3f}".format(SolucionExacta))
    print("--"*20)
    
